#!/usr/bin/env python
#
# Auto-reload web page in browser when changes are made to files
#
# Usage:
#   python3 -m venv venv
#   venv/bin/pip install -r requirements.txt
#   venv/bin/python serve.py

from pathlib import Path

from livereload import Server

base_dir = Path(__file__).parent.resolve()

server = Server()
server.watch(str(base_dir / '*.html'))
server.watch(str(base_dir / 'css' / '*.css'))
server.watch(str(base_dir / 'js' / '*.js'))
server.watch(str(base_dir / 'images' / '*.svg'))

server.serve()
