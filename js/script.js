(function(fn) {
  if (document.readyState !== 'loading') {
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
})(function(){
  var sprite_sheet;
  var sprites_url = 'images/sprites.svg';

  function download(blob) {
    var node = document.createElement('a');
    node.href = URL.createObjectURL(blob);
    node.download = 'icons.svg';
    document.body.appendChild(node);
    node.click();
    document.body.removeChild(node);
  };

  function loadSVG(url, callback) {
    var request = new XMLHttpRequest();
    request.onload = function() {
      if (this.status >= 200 && this.status < 400){
        callback(this.response, request);
      } else {
        console.log('Error: unable to GET ' + url, this);
      }
    }
    request.onerror = function() {
      console.log('Error: unable to GET ' + url, this);
    }
    request.open('GET', url);
    request.send();
  };

  // init page
  loadSVG(sprites_url, function(response) {
    var loading = document.getElementById('loading');
    loading.parentNode.removeChild(loading);

    var parser = new DOMParser()
    sprite_sheet = parser.parseFromString(response, 'image/svg+xml');

    var icons = document.querySelector('#icons');
    icon_prefix = '<svg class="svi"><use href="';
    icon_infix = '"></use></svg><small>';
    icon_suffix = '</small>';

    sprite_sheet.querySelectorAll('symbol').forEach(function(item) {
      var icon = document.createElement('div')
      icon.className = 'icon'
      icon.innerHTML = icon_prefix + sprites_url + '#' + item.id
                       + icon_infix + item.id + icon_suffix;

      icon.addEventListener('click', function() {
        icon.dataset.selected = (icon.dataset.selected !== 'true');
      });
      icon.addEventListener('mouseenter', function() {
        icon.classList.add('hover');
      });
      icon.addEventListener('mouseleave', function() {
        icon.classList.remove('hover');
      });

      icons.appendChild(icon);
    });
  });

  document.getElementById('download').addEventListener('click', function() {
    var symbols = []

    document.querySelectorAll('.icon[data-selected="true"]').forEach(function(item) {
      var svg_use = item.querySelector('svg use');

      if (svg_use === null) {
        return;
      }

      var [url, id] = svg_use.getAttribute('href').split('#');
      symbols.push(sprite_sheet.getElementById(id));
    });

    var svg = document.createElement('svg');
    svg.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

    symbols.forEach(function(symbol) {
      svg.appendChild(symbol.cloneNode(true));
    });

    if (document.getElementById('previews').checked) {
      svg.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');
      var grid_size = Math.ceil(Math.sqrt(symbols.length));
      symbols.forEach(function(symbol, i) {
        var use = document.createElement('use');
        use.setAttribute('xlink:href', '#' + symbol.id);
        use.setAttribute('x', i % grid_size * 100);
        use.setAttribute('y', Math.floor(i / grid_size) * 100);
        use.setAttribute('width', 100);
        use.setAttribute('height', 100);
        svg.appendChild(use);
      });
    }

    var svg_str = '<?xml version="1.0" encoding="utf-8"?>' + svg.outerHTML;
    svg_str = svg_str.replace('\n', '').replace(/>\s+</g, '><');
    download(new Blob([svg_str], {type: 'image/svg'}));
  });
});
